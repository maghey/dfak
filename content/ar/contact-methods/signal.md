---
layout: page
title: Signal
author: mfc
language: ar
summary: وسائل الاتصال
date: 2018-09
permalink: /ar/contact-methods/signal.md
parent: /ar/
published: true
---

استعمال تطبيق سِجنَل (Signal) للهواتف الذكيَّة يضمن سريَّة فحوى رسائلك بتعميتها إلى المتلقّي، و&nbsp;لَنْ يعلم سواكما بتواصلكما، لكنْ تنبغي ملاحظة أنَّ سِجنَل يستعمل أرقام الهواتف كمُعرِّفات للمستخدمين، لذا فسيكون عليك مشاركة رقم هاتفك مع كل مَنْ تتواصل معه.

موارد: [How to: Use Signal for Android](https://ssd.eff.org/en/module/how-use-signal-android), [How to: Use Signal for iOS](https://ssd.eff.org/en/module/how-use-signal-ios), [How to Use Signal Without Giving Out Your Phone Number](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)
