---
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: ddhs, tsd
hours: Segunda-Sexta, 24/5, UTC-4
response_time: 6 horas
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect é um serviço livre e gratuito de segurança de sites em defesa de grupos da sociedade civil e de direitos humanos contra ataques digitais.
