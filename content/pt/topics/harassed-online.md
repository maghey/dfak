---
layout: page
title: "Você está sendo alvo de perseguição online?"
author: Floriana Pagano
language: pt
summary: "Você está sendo alvo de perseguição online?"
date: 2019-04-01
permalink: /pt/topics/harassed-online/
parent: /pt/
---

# Você está sendo alvo de perseguição online?

A Internet, e as plataformas de mídia social em particular, tem se tornado um espaço crítico para membros da sociedade civil e organizações, especialmente mulheres, pessoas LGBTQIA. pessoas negras, e outras maiorias silenciadas, para expressarem-se e fazerem suas vozes ouvidas. Mas ao mesmo tempo, também se tornam espaços onde estes grupos são facilmente focados por expressarem suas perspectivas. Violência e abuso impedem estas e muitas outras pessoas desprovidas de privilégios do direito à liberdade de expressão igualitária e sem medo de retalições.

Violência e assédio online possuem diferentes formas, e agentes maliciosos comumente confiam na impunidade de seus atos devido à ausência de leis que protejam vítimas de perseguição em diversos países, mas especialmente pelo fato de tais ataques possuem naturezas específicas, e dependem de estratégias criativas e personalizadas de proteção para que sejam evitados.

Por isso, é importante identificar a tipologia dos ataques contra você, para decidir quais medidas tomar.

Esta seção do Kit de Primeiros Socorros Digitais irá trilhar com você através dos passos básicos para planejar como se proteger contra ataque que estiver sofrendo.

Se você for o alvo de perseguição online neste momento, siga o questionário para identificar a natureza do problema e tentar encontrar possíveis soluções.

## Workflow

### physical-wellbeing

Você teme por sua integridade física e bem estar?

- [Sim](#physical-risk_end)
- [Não](#no-physical-risk)

### no-physical-risk

Você acha que o atacante acessou ou está acessando seu dispositivo?

 - [Sim](#device-compromised)
 - [Não](#account-compromised)

### device-compromised

> Troque sua senha de acesso do dispositivo: prefira usar uma frase com palavras aleatórias, que não digam respeito a algo pessoal, usando espaços e  caracteres especiais fáceis de lembrar. Mais dicas [neste link](https://ssd.eff.org/pt-br/module/criando-senhas-fortes).
>
> - [Mac OS](https://support.apple.com/pt-br/HT202860)
> - [Windows](https://support.microsoft.com/pt-br/help/14087/windows-7-change-your-windows-password)
> - [iOS - Apple ID](https://support.apple.com/pt-br/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en)

A pessoa que te invadiu te trancou para fora do aparelho sem acesso?

 - [Sim](#account-compromised)
 - [Não](../../../device-acting-suspiciously)

### account-compromised

> Se alguém conseguiu acessar seu dispositivo, possivelmente a este ponto também deve ter acessado suas contas online, acessado seus contatos e suas mensagens, publicar mensagens, imagens e vídeos se passando por você.

Você notou postagens ou mensagens desaparecendo da sua conta, ou outras atividades que te dão motivos para acreditar que sua conta foi comprometida? Revise também a pasta de itens enviados (nos emails e nas mensagens de chat) para confirmar possíveis atividades suspeitas.

 - [Sim](../../../account-access-issues)
 - [Não](#impersonation)

### impersonation

Há alguém se passando por você?

- [Sim](../../../impersonated)
- [Não](#doxing)

### doxing

Tem alguém publicando informação pessoal ou fotos suas sem seu consentimento?

- [Sim](#doxing-yes)
- [Não](#hate-speech)

### doxing-yes

Onde sua informação pessoal ou fotos foram publicadas?

- [Em uma plataforma de rede social](#doxing-sn)
- [Em um site/blog](#doxing-web)

### doxing-sn

> Se a sua informação pessoal ou suas fotos foram publicadas em uma plataforma de mídia social, você pode denunciar a violação dos padrões de comunidade seguindo os procedimentos de denúncia fornecidos pelos sites das redes sociais a seus usuários. Você deve encontrar instruções para as principais plataformas na lista a seguir:
>
> - [Google](https://www.cybercivilrights.org/online-removal/#google)
> - [Facebook](https://www.cybercivilrights.org/online-removal/#facebook)
> - [Twitter](https://www.cybercivilrights.org/online-removal/#twitter)
> - [Tumblr](https://www.cybercivilrights.org/online-removal/#tumblr)
> - [Instagram](https://www.cybercivilrights.org/online-removal/#instagram)

A informação ou mídia sobre você foi deletada?

 - [Sim](#one-more-persons)
 - [Não](#harassment_end)

### doxing-web

> Siga as instruções em ["Acoso Online - Denuncie um Caso"](https://acoso.online/br/reporta-el-caso-en-las-plataformas-de-internet/) para remover conteúdo de um site.

O conteúdo foi tirado do site?

- [Sim](#one-more-persons)
- [Não](#harassment_end)

### hate-speech

Este ataque é focado em raça, gênero ou religião?

- [Sim](#one-more-persons)
- [Não](#harassment_end)


### one-more-persons

O ataque foi feito por uma ou mais pessoas?

- [Uma pessoa](#one-person)
- [Mais de uma pessoa](#more-persons)

### one-person

Você conhece esta pessoa?

- [Sim](#known-harasser)
- [Não](#block-harasser)


### known-harasser

> Se você conhece a pessoa que está te assediando, pode pensar em denunciá-la para as autoridades locais, se aplicável ao seu caso. Cada país tem leis diferentes para proteger pessoas em situações de assédio e perseguição online, e você irá precisar explorar as possibilidades da legislação local para decidir a melhor forma de proceder.
>
> Em caso de optar por um processo contra a pessoa, é importante contar com apoio jurídico.

Você quer processar o agressor?

 - [Sim](#legal_end)
 - [Não](#block-harasser)


### block-harasser

> Sabendo ou não quem é a pessoa que está agredindo, é sempre uma boa ideia bloqueá-la nas plataformas de mídia social quando possível.
>
> - [Facebook](https://pt-br.facebook.com/help/290450221052800)
> - [Twitter](https://help.twitter.com/pt/using-twitter/blocking-and-unblocking-accounts)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=pt-br)
> - [Tumblr](https://tumblr.zendesk.com/hc/pt-br/articles/231877648-Blocking-users)
> - [Instagram](https://help.instagram.com/426700567389543)

Conseguiu bloquear o assediador efetivamente?

 - [Sim](#resolved_end)
 - [Não](#harassment_end)


### more-persons

> Se mais de uma pessoa está te atacando, você pode estar sendo alvo de uma campanha de perseguição, e vai precisar refletir qual a melhor estratégia a tomar no seu caso.
>
> Para aprender sobre todas as possíveis estratégias, leia [a página da Take Back The Tech (em espanhol): Discurso de odio: estrategias](https://www.takebackthetech.net/es/be-safe/discurso-de-odio-estrategias). Um documento abrangente sobre o tema em português pode ser encontrado [neste link](https://rm.coe.int/portuguese-manual-alternativas/16808e95e3).

Conseguiu identificar a melhor estratégia para você?

 - [Sim](#resolved_end)
 - [Não](#harassment_end)

### harassment_end

> Se ainda está sob assédio e precisa de uma solução especializada, por favor contate as organizações abaixo que poderão auxiliar você.

:[](organisations?services=harassment)


### physical-risk_end

> Se você se encontra sob ameaça física, contate as organizações abaixo que poderão auxiliar você.

:[](organisations?services=physical_security)


### legal_end

> Se você precisa de apoio jurídico, por favor contate as organizações abaixo que podem auxiliar você.

:[](organisations?services=legal)

### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Documente o assédio:** É muito útil documentar os ataques e outros incidentes que você possa estar presenciando: tire prints (capturas de tela), guarde as mensagens que recebe dos perseguidores etc. Se possível, mantenha um catálogo onde você possa manter datas, horários, plataformas e locais online (grupos, páginas etc), usuários, prints, descrição dos eventos e tudo o que for possível. Este tipo de catálogo pode ajudar a detectar padrão e indicar atacantes, além de poder ser necessário na legislação vigente para abertura de um possível processo. Manter tudo isso pode demandar muito emocionalmente de você, e se for o caso, pense em alguém que você confia profundamente para ajudar você durante este momento. É importante confiar bastante nesta pessoa pois ela irá precisar de acesso a dados pessoais e às suas contas online.  Antes de compartilhar sua senha com esta pessoa, troque-a para algo diferente da que você utilizava, e compartilhe apenas através de vias seguras - como [ferramentas de comunicação com criptografia de ponta-a-ponta](https://www.frontlinedefenders.org/pt/resource-publication/guide-secure-group-chat-and-conferencing-tools). Assim que se sentir confiante novamente para retomar este processo, não se esqueça de trocar as suas senhas novamente usando frases únicas e [seguras](https://ssd.eff.org/pt-br/module/criando-senhas-fortes) que só você possa saber.

  - Você poderá encontrar instruções sobre como documentar incidentes no artigo [Como documentar casos de violência de gênero na Internet de forma empática e segura?](https://acoso.online/wp-content/uploads/2020/10/como-documentar_portugues.pdf).

- **Configure a autenticação em dois fatores** em todas as suas contas. A autenticação em dois fatores pode ser muito efetiva para barrar alguém de acessar suas contas sem a sua permissão. Se você puder escolher, não use a autenticação baseada em SMS, prefira usar opções baseadas em aplicativos ou em uma chave de segurança.

    - Se não souber qual a melhor solução para você, confira o infográfico [Access Now's "What is the best type of multi-factor authentication for me?" infographic](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) e também ["Avast: Guia essencial de segurança: a autenticação multifator"](https://blog.avast.com/pt-br/guia-essencial-de-seguranca-a-autenticacao-multifator).
    - Você encontrará instruções para configurar a verificação em duas etapas para as plataformas mais populares no artigo [Como ativar verificação de duas etapas nos principais serviços que você usa](https://tecnoblog.net/211532/ativar-verificacao-duas-etapas-dois-fatores/).

- **Mapeie sua presença online**. Use a inteligência de código aberto disponível na internet para buscar informações disponíveis sobre você e interprete de forma estratégica para conseguir prevenir atores maliciosos de usarem estas informações para se passar por você.


#### Resources

- [Doxxing: Mini guia para prevenção e redução de danos](https://gus.computer/blog/2020/06/04/doxxing/)
- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Access Now Helpline Community Documentation: FAQ - Online Harassment Targeting a Civil Society Member](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)​​​​​​​
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Como documentar casos de violência de gênero na Internet de forma empática e segura?](https://acoso.online/wp-content/uploads/2020/10/como-documentar_portugues.pdf)
