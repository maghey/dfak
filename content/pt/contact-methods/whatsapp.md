---
layout: page
title: WhatsApp
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/whatsapp.md
parent: /pt/
published: true
---

Usar o WhatsApp garante que suas conversas com o destinatário serão protegidas de forma que apenas vocês possam ler as comunicações, porém, governos e autoridades na sua jurisdição e da pessoa receptora da mensagem respaldadas a serem notificadas sobre as suas comunicações.

Resources: [Como Usar WhatsApp no Android](https://ssd.eff.org/pt-br/module/how-use-whatsapp-android) [Como Usar WhatsApp no iOS](https://ssd.eff.org/pt-br/module/how-use-whatsapp-ios).
