---
layout: page
title: "¿Estás siendo acosado o acosada en línea?"
author: Floriana Pagano
language: es
summary: "¿Estás siendo acosado o acosada en línea?"
date: 2019-08
permalink: /es/topics/harassed-online/
parent: /es/

---

# ¿Estás siendo acosado o acosada en línea?

La Internet, y en particular las redes sociales, se han convertido en un espacio crítico para que los miembros y las organizaciones de la sociedad civil, especialmente las mujeres, las personas LGBTIQ y otras minorías, se expresen y hagan oír su voz. Pero al mismo tiempo, también se han convertido en espacios donde estos grupos son fácilmente señalados por expresar sus opiniones. La violencia y el abuso en línea les niegan a las mujeres, a las personas LGBTIQ y a muchas otras personas desprivilegiadas el derecho a expresarse por igual, libremente y sin temor.

La violencia y el abuso en línea se pueden encontrar en muchas formas diferentes, y las entidades malintencionadas a menudo pueden respaldarse no solo en la impunidad, debido a la falta de leyes en muchos países que protejan a las víctimas de acoso, sino también porque las estrategias de protección a la victimas deben ajustarse de manera creativa dependiendo de qué tipo de ataque se ha lanzado.

Por lo tanto, es importante identificar la tipología del ataque dirigido para decidir qué pasos se pueden tomar.

Esta sección del kit de primeros auxilios digitales te guiará a través de algunos pasos básicos para planificar cómo protegerte contra el ataque que estás sufriendo.

Si eres blanco de acoso en línea, sigue este cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.

## Workflow

### physical-wellbeing

¿Temes por tu integridad física o bienestar?

- [Sí](#physical-risk_end)
- [No](#no-physical-risk)

### no-physical-risk

¿Crees que el atacante ha accedido o está accediendo a tu dispositivo?

 - [Sí](#device-compromised)
 - [No](#account-compromised)

### device-compromised

> Cambia la contraseña para acceder a tú dispositivo por otra contraseña única, larga y compleja:
>
> - [Mac OS](https://support.apple.com/es-lamr/HT202860)
> - [Windows](https://support.microsoft.com/es-us/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/es-lamr/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=es)

¿Se ha bloqueado efectivamente al atacante de tu dispositivo?

 - [Sí](#account-compromised)
 - [No](../../../device-acting-suspiciously)

### account-compromised

> Si alguien tuvo acceso a tu dispositivo, es posible que también hayan accedido a tus cuentas en línea, por lo que podría estar leyendo tus mensajes privados, identificando tus contactos y haciendo publicaciones de imágenes o videos haciéndose pasar por ti.

¿Has notado la desaparición de publicaciones o mensajes, u otras actividades que te den una buena razón para pensar que tu cuenta ha sido comprometida? Revisa también tus carpetas de mensajes enviados por posibles actividades sospechosas.

 - [Sí](../../../account-access-issues)
 - [No](#impersonation)

### impersonation

¿Alguien se está haciendo pasar por ti?

- [Sí](../../../impersonated)
- [No](#doxing)

### doxing

¿Alguien ha publicado información privada o fotos sin tu consentimiento?

- [Sí](#doxing-yes)
- [No](#hate-speech)

### doxing-yes

¿Dónde se ha publicado tu información privada o imágenes?

- [En una plataforma de redes sociales](#doxing-sn)
- [En un sitio web](#doxing-web)

### doxing-sn

> Si tu información privada o tus imágenes se han publicado en una plataforma de redes sociales, puedes reportar una violación de los estándares de la comunidad siguiendo los procedimientos de proporcionados a los usuarios por los sitios web de redes sociales. Encontrarás instrucciones para las plataformas principales en [este enlace](https://acoso.online/cl/reporta-el-caso-en-las-plataformas-de-internet/).

¿Se pudo eliminar la información o los contenidos multimedia?

 - [Sí](#one-more-persons)
 - [No](#harassment_end)

### doxing-web

> Sigue las instrucciones en ["Without My Consent - Take Down" (En Inglés)](https://withoutmyconsent.org/resources/take-down)
para eliminar contenido de un sitio web.

¿El contenido ha sido retirado por el sitio web?

- [Sí](#one-more-persons)
- [No](#harassment_end)

### hate-speech

¿El ataque se basa en atributos como la raza, el género o la religión?

- [Sí](#one-more-persons)
- [No](#harassment_end)


### one-more-persons

¿Has sido atacado por una o más personas?

- [One person](#one-person)
- [More persons](#more-persons)

### one-person

¿Conoces a esta persona?

- [Sí](#known-harasser)
- [No](#block-harasser)


### block-harasser

>Si sabes quién te está acosando, puedes considerar informar a las autoridades de tu país. Cada país tiene leyes diferentes para proteger a las personas del acoso en línea, por lo que debes explorar la legislación en tu país para decidir qué hacer.
>
> Si decides demandar a esta persona, debes comunicarse con un experto legal.

¿Quieres demandar al atacante?

 - [Sí](#legal_end)
 - [No](#block-harasser)


### block-harasser

> Ya sea que sepas quién es tu acosador o no, siempre es una buena idea bloquearlos en las plataformas de redes sociales siempre que sea posible.
>
> - [Facebook](https://www.facebook.com/help/290450221052800)
> - [Twitter](https://help.twitter.com/es/using-twitter/blocking-and-unblocking-accounts)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=es)
> - [Tumblr](https://tumblr.zendesk.com/hc/es/articles/231877648-Bloquear-a-otros-usuarios)
> - [Instagram](https://help.instagram.com/426700567389543)

¿Has bloqueado a tu acosador de manera efectiva?

 - [Sí](#resolved_end)
 - [No](#harassment_end)


### more-persons

> Si estás siendo atacado por más de una persona, podrías ser el objetivo de una campaña de acoso y deberás reflexionar sobre cuál es la mejor estrategia que se aplica a tu caso.
>
> Para conocer todas las estrategias posibles, lee la [página de "Take Back The Tech" sobre estrategias contra el discurso de odio](https://www.takebackthetech.net/es/be-safe/discurso-de-odio-estrategias)

¿Has identificado la mejor estrategia para ti?

 - [Sí](#resolved_end)
 - [No](#harassment_end)

### harassment_end

> Si aún estás bajo acoso y necesitas una solución personalizada, por favor comunícate con las organizaciones descritas a continuación que pueden ayudarte.

:[](organisations?services=harassment)


### physical-risk_end

> Si estás en riesgo físico, comunícate con las organizaciones descritas a continuación que pueden ayudarte.

:[](organisations?services=physical_security)


### legal_end

> Si necesitas asistencia legal, comunícate con las organizaciones descritas a continuación que pueden ayudarte.

:[](organisations?services=legal)

### resolved_end

Esperamos que esta guía de solución de problemas te haya sido útil. Envíanos tus comentarios [por correo electrónico](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Consejos_finales

- **Documentar acoso:** Es útil documentar los ataques o cualquier otro incidente que puedas estar presenciando: tomar capturas de pantalla, guardar los mensajes que recibes de los acosadores, etc. Si es posible, crea un diario donde puedas sistematizar esta documentación registrando fechas, horas, plataformas y URLs, ID de usuario, descripción de lo sucedido, etc. Los diarios pueden ayudarte a detectar posibles patrones e indicaciones sobre tus atacantes. Si te siente abrumada/o, intenta pensar en alguien en quien confíes que pueda documentar dichos incidentes por ti durante un tiempo. Debes confiar profundamente en la persona que administrará esta documentación, ya que deberás entregarle las credenciales de tus cuentas personales. Antes de compartir tu contraseña con esta persona, cámbiala por otra diferente y compártela a través de un medio seguro, como una herramienta con [cifrado de extremo a
extremo](https://www.frontlinedefenders.org/es/resource-publication/guide-secure-group-chat-and-conferencing-tools). Una vez que sientas que puedes recuperar el control de la cuenta, recuerda volver a cambiar tu contraseña a algo único, [seguro](https://ssd.eff.org/es/module/creando-contrase%C3%B1as-seguras), y que solo tu sepas.

    - Puedes encontrar instrucciones sobre cómo documentar el incidente en estos [Consejos de documentación para sobrevivientes de abuso y acoso a través de tecnología\](https://www.techsafety.org/sugerencias-sobre-la-documentacion-para-sobrevivientes-del-abuso-tecnolgico-y-acecho).

- **Configura la autenticación en 2 pasos** en todas tus cuentas. La autenticación en 2 pasos puede ser muy efectiva para evitar que alguien acceda a tus cuentas sin tu permiso. Si puedes elegir, no utilices la autenticación de 2 pasos basada en SMS y elige una opción diferente, basada en una aplicación de teléfono o en una clave de seguridad.

    - Si no sabes qué solución es la mejor para ti, puedes consultar la infografia de Access Now []"¿Cuál es el mejor tipo de autenticación multifactor para mí?"](https://www.accessnow.org/cms/assets/uploads/2017/12/2FA-infographic-ESP.png) y la [Guía del EFF, sobre los tipos comunes de autenticación de dos factores en la Web" (En Inglés)](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - Puedes encontrar instrucciones para configurar la autenticación de dos factores en las principales plataformas en [The 12 Days of 2FA: How to Enable Two-Factor Authentication For Your Online Accounts (En Inglés)](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Mapea tu presencia en línea**. El auto doxing consiste en explorar la inteligencia de código abierto sobre nosotros mismos para evitar que actores maliciosos encuentren y utilicen esta información para hacerse pasar por ti.


#### Resources

- [Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: Guía para prevenir el Doxing - En Inglés](https://guides.accessnow.org/self-doxing.html)
- [Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: FAQ - Acoso en línea dirigido a un miembro de la sociedad civil - En Inglés](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)​​​​​​​
- [Acoso.Online](https://acoso.online)
- [Cloud seguro: ¿Qué hacer si soy víctima de Ciberacoso?](https://www.cloudseguro.co/victima-de-ciberacoso/)
- [Equality Labs: Guía anti-doxing para activistas que enfrentan ataques desde la Alt-derecha (En Inglés)](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Bloqueando tu identidad digital (En Inglés)](http://femtechnet.org/csov/lock-down-your-digital-identity/)
​​​​​​​- [National Network to End Domestic Violence: Sugerencias sobre la Documentación para Sobrevivientes del Abuso Tecnológico y
Acecho](https://www.techsafety.org/sugerencias-sobre-la-documentacion-para-sobrevivientes-del-abuso-tecnolgico-y-acecho)
- [Stop bullying: Reportar casos de ciberacoso](https://espanol.stopbullying.gov/acoso-por-internet/c%C3%B3mo-denunciar-129k/%C3%ADndice.html)
