---
layout: page
title: PGP
author: mfc, Metamorphosis Foundation
language: sq
summary: Metodat e kontaktimit
date: 2018-09
permalink: /sq/contact-methods/pgp.md
parent: /sq/
published: true
---

PGP (ose Pretty Good Privacy) dhe ekuivalenti i tij me burim të hapur, GPG (Gnu Privacy Guard), ju mundëson të kriptoni përmbajtjen e e-maileve për të mbrojtur mesazhin tuaj që të mos shihet nga ofruesi juaj i internetit ose ndonjë palë tjetër që mund të ketë qasje në e-mail. Megjithatë, fakti që ju i keni dërguar një mesazh organizatës marrëse mund të jetë i arritshëm nga qeveritë ose organet e zbatimit të ligjit. Për të parandaluar këtë, mund të krijoni një adresë alternative të e-mailit që nuk lidhet me identitetin tuaj.

Burime: [Encrypting Email with Mailvelope: A Beginner's Guide](https://freedom.press/training/encrypting-email-mailvelope-beginners-guide/)
