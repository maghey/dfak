---
layout: page
title: "Kam humbur të dhënat e mia"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof, Metamorphosis Foundation
language: sq
summary: "Çfarë duhet të bëni në rast se keni humbur disa të dhëna"
date: 2019-03-12
permalink: /sq/topics/lost-data
parent: Home
---

# Kam humbur të dhënat e mia

Të dhënat digjtale mund të jenë shumë efemerale dhe të paqëndrueshme dhe ka shumë mënyra se si mund t’i humbni ato. Dëmtimi fizik i pajisjeve tuaja, ndërprerja e llogarive tuaja, fshirja e gabuar, azhurnimet e programeve kompjuterike dhe ndërprerjet aksidentale të programeve kompjuterike mund të shkaktojnë humbje të të dhënave. Për më tepër, ndonjëherë mund të mos jeni të vetëdijshëm se si funksionon sistemi juaj i kopjeve rezervë, ose thjesht i keni harruar kredencialet e llogarive tuaja ose mënyrën për të gjetur ose për të rikuperuar të dhënat tuaja.

Kjo pjesë e Veglërisë së ndihmës së parë digjitale do t'ju njoftojë me disa hapa themelorë për të diagnostikuar si mund t'i keni humbur të dhënat dhe strategjitë e mundshme zbutëse për t'i rikuperuar ato.

Në vazhdim është një pyetësor për ta identifikuar natyrën e problemit tuaj dhe për të gjetur zgjidhje të mundshme.

## Workflow

### entry-point

>Në këtë pjesë kryesisht fokusohemi në të dhënat e bazuara në pajisje. Për përmbajtjet online dhe kredencialet e llogarive, do t'ju drejtojmë tek pjesët tjera të Veglërisë së ndihmës së parë digjitale.
>
> Pajisjet përfshijnë kompjuterë, pajisje celulare, disqe të jashtme të ngurta, memorie USB dhe karta SD.

Çfarë lloj të dhënash keni humbur?

- [Përmbajtje online (në internet)](../../../account-access-issues)
- [Kredencialet](../../../account-access-issues)
- [Përmbajtje në pajisje](#content-on-device)

### content-on-device

> Shpesh të dhënat tuaja "të humbura" janë të dhëna që thjesht janë fshirë - rastësisht ose me qëllim. Në kompjuterin tuaj kontrolloni Recycle Bin ose Trash. Në pajisjet Android, mund t'i gjeni të dhënat e humbura në dosjen LOST.DIR. Nëse skedarët e zhdukur ose të humbur nuk mund të gjenden në Recycle Bin ose Trash të sistemit tuaj operativ, është e mundur që ato të mos jenë fshirë, por të gjenden në një lokacion tjetër nga ai që ju e prisni. Provoni funksionin e kërkimit në sistemin tuaj operativ për t'i gjetur ato.
>
> Kontrolloni gjithashtu skedarët dhe dosjet e fshehura, pasi të dhënat që keni humbur mund të jenë aty. Ja se si mund ta bëni këtë në [Mac](https://www.macworld.co.uk/how-to/mac-software/show-hidden-files-mac-3520878/) dhe [Windows](https://support.microsoft.com/en-ie/help/4028316/windows-view-hidden-files-and-folders-in-windows-10). Për Linux, shkoni te dosja juaj themelore në menaxherin e skedarëve dhe shtypni Ctrl + H. Skedarët e fshehur mund t'i shihni edhe në Android dhe iOS.
>
> Provoni të kërkoni emrin e saktë të skedarit të humbur. Nëse kjo nuk funksionon, ose nëse nuk jeni të sigurt për emrin e saktë, mund të provoni "kërkimin me xhoker" (d.t.th. nëse keni humbur një skedar docx, por nuk jeni të sigurt për emrin, mund të kërkoni `*.docx`), që do t'i nxjerrë vetëm skedarët me shtesën `.docx`. Renditni sipas *Datës së Ndryshimit* në mënyrë që të gjeni shpejt skedarët më të fundit duke përdorur opsionin e "kërkimit me xhoker".
>
> Përveç kërkimit të të dhënave të humbura në pajisjen tuaj, pyesni veten nëse i keni dërguar ato me e-mail ose i keni ndarë me dikë (duke përfshirë veten tuaj) ose i keni shtuar ato në hapësirën tuaj në re (cloud) në çfarëdo momenti. Nëse rasti është i tillë, mund të bëni një kërkim atje dhe mund të jeni në gjendje t'i riktheni ato ose ndonjë version të tyre.
>
> Për të rritur shanset për rikuperimin e të dhënave, ndaloni menjëherë përdorimin e pajisjeve tuaja. Shkrimi i vazhdueshëm në "hard disk" mund të zvogëlojë shanset për të gjetur/rikthyer të dhënat.

Si i keni humbur të dhënat?

- [Pajisja ku janë ruajtur të dhënat pësoi një dëmtim fizik](#tech-assistance_end)
- [Pajisja ku ruheshin të dhënat ishte vjedhur/humbur](#device-lost-theft_end)
- [Të dhënat u fshinë](#where-is-data)
- [Të dhënat u zhdukën pas një azhurnimi të softuerit](#software-update)
- [Sistemi ose mjeti softuerik pësoi ndërprerje aksidentale dhe të dhënat u zhdukën](#where-is-data)


### software-update

> Ndonjëherë, kur azhurnoni ose përmirësoni ndonjë vegël softuerike ose të gjithë sistemin operativ, mund të ndodhin probleme të papritura, duke bërë që sistemi ose softueri të mos funksionojë siç duhet, ose të ndodhë ndonjë sjellje e gabuar, duke përfshirë dëmtimin e të dhënave. Nëse keni vërejtur ndonjë humbje të të dhënave menjëherë pas azhurnimit të softuerit ose sistemit, ia vlen të merret parasysh që të ktheheni në një gjendje të mëparshme. Kthimet në gjendje të mëparshme janë të dobishme sepse ato nënkuptojnë që softueri juaj ose baza juaj e të dhënave mund të rikthehen në një gjendje të pastër dhe të qëndrueshme edhe pasi të kenë ndodhur operacione të gabuara ose ndërprerje aksidentale të softuerit.
>
> Metoda për të rikthyer një pjesë të softuerit në një version të mëparshëm varet nga mënyra se si është ndërtuar ai softuer - në disa raste kjo mund të bëhet lehtësisht, në raste të tjera nuk është aq lehtë, ndërsa disa të tjerë kërkojnë pak punë në lidhje me të. Kështu që do t'ju duhet të bëni një kërkim në internet për t'u rikthyer në versionet më të vjetra për atë softuer të caktuar. Ju lutemi vini re se kthimi mbrapa është i njohur edhe si "downgrading", kështu që bëni një kërkim edhe me këtë term. Për sistemet operative të tilla si Windows ose Mac, secili i ka metodat e veta për kthimin në versionin e mëparshëm.
>
> - Për sistemet Mac, vizitoni [Mac Support Center knowledge base](https://support.apple.com) dhe përdorni fjalën "downgrade" me versionin tuaj macOS për të lexuar më shumë dhe për të gjetur udhëzime.
> - Për sistemet Windows, procedura ndryshon në varësi të asaj se a dëshironi ta anuloni ndonjë azhurnim specifik ose ta riktheni gjithë sistemin. Në të dyja rastet mund të gjeni udhëzime në [Microsoft Support Center](https://support.microsoft.com), për shembull për Windows 10 mund të kërkoni "How do I remove an installed update" në këtë [faqe të Pyetjeve të Shpeshta](https://support.microsoft.com/en-us/help/12373/windows-update-faq).

A ishte i dobishëm rikthimi i softuerit?

- [Po](#resolved_end)
- [Jo](#where-is-data)


### where-is-data

> Bërja e rregullt e kopjeve rezervë të të dhënave është një praktikë e mirë dhe e rekomanduar. Ndonjëherë harrojmë se kemi aktivizuar krijimin automatik të kopjeve rezervë, kështu që ia vlen të kontrolloni pajisjet tuaja nëse e keni aktivizuar atë opsion dhe përdorni atë kopje rezervë për të rikthyer të dhënat tuaja. Në rast se nuk e keni bërë këtë, rekomandohet planifikimi për të bërë kopje rezervë në të ardhmen, dhe mund të gjeni më shumë këshilla se si të krijoni kopje rezervë në [këshillat përfundimtare të kësaj rrjedhe pune](#resolved_end).

Për të kontrolluar nëse keni kopje rezervë të të dhënave që i keni humbur, filloni të pyesni veten se ku ishin ruajtur të dhënat e humbura.

- [Në pajisje të ruajtjes (hard disk i jashtëm, memorie USB, karta SD)](#storage-devices_end)
- [Në kompjuter](#computer)
- [Në pajisje celulare](#mobile)

### computer

Çfarë lloj sistemi operativ keni në kompjuterin tuaj?

- [MacOS](#macos-computer)
- [Windows](#windows-computer)
- [Gnu/Linux](#linux-computer)

### macos-computer

> Për të kontrolluar nëse pajisja juaj MacOS kishte të aktivizuar opsionin për të krijuar kopje rezervë dhe për ta përdorur atë kopje rezervë për t'i rikthyer të dhënat tuaja, kontrolloni opsionet tuaja në [[iCloud](https://support.apple.com/en-us/HT208682) ose [Time Machine](https://support.apple.com/en-za/HT201250) options për të parë nëse ka ndonjë kopje rezervë të disponueshme.
>
> Një vend për të parë është lista "Recent items", që i ndjek aplikacionet, skedarët dhe serverët që i keni përdorur gjatë disa seancave tuaja të kaluara në kompjuter. Për të kërkuar skedarin dhe për ta rihapur atë, shkoni te menyja Apple në këndin e sipërm të majtë, zgjidhni "Recent items" dhe shfletoni listën e skedarëve. Më shumë detaje mund të gjenden [këtu](https://www.nytimes.com/2018/08/01/technology/personaltech/mac-find-lost-files.html).

A arritët t'i gjeni të dhënat tuaja ose t'i riktheni ato?

- [Po](#resolved_end)
- [Jo](#tech-assistance_end)

### windows-computer

> Për të kontrolluar nëse e keni të aktivizuar opsionin për të bërë kopje rezervë në makinën tuaj me Windows, mund të lexoni [këto udhëzime](https://support.microsoft.com/en-us/help/4027408/windows-10-backup-and-restore).
>
> Windows 10 përfshin tiparin **Timeline**, i cili ka për qëllim të përmirësojë produktivitetin tuaj duke mbajtur shënim të skedarëve që keni përdorur, ueb-faqeve që keni shfletuar dhe veprimeve të tjera që i keni bërë në kompjuterin tuaj. Nëse nuk mbani mend se ku e keni ruajtur një dokument, mund të klikoni në ikonën "Timeline" në shiritin e detyrave në Windows 10 për të parë një regjistër vizual të organizuar sipas datës, dhe të kaloni përsëri në atë që ju nevojitet duke klikuar ikonën e duhur të parapamjes. Kjo mund të ju ndihmojë të gjeni një skedar që është riemëruar. Nëse "Timeline" është aktivizuar, disa nga aktivitetet e PC-së suaj - si skedarët që redaktoni në Microsoft Office - mund të sinkronizohen me pajisjen tuaj celulare ose me një kompjuter tjetër që përdorni, kështu që mund të keni një kopje rezervë të të dhënave tuaja të humbura në një pajisje tjetër. Mund të lexoni më shumë për "Timeline" dhe si ta përdorni atë [këtu](https://www.nytimes.com/2018/07/05/technology/personaltech/windows-10-timeline.html).

A arritët t'i gjeni të dhënat tuaja ose t'i riktheni ato?

- [Po](#resolved_end)
- [Jo](#windows-restore)

### windows-restore

> Në disa raste ka mjete falas dhe me burim të hapur që mund të jenë të dobishme për të gjetur përmbajtjen që mungon dhe për ta rikuperuar atë. Ndonjëherë ato janë të kufizuara në përdorim, dhe shumica e mjeteve të njohura kushtojnë para.
>
> Për shembull [Recuva](http://www.ccleaner.com/recuva ) është softuer i mirënjohur falas për rikuperim. Ne ju rekomandojmë t'i jepni një shans dhe të shihni se si shkon.

A ishte i dobishëm ky informacion për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

- [Po](#resolved_end)
- [Jo](#tech-assistance_end)


### linux-computer

> Disa nga versionet më të njohura Linux, si Ubuntu, kanë një mjet të integruar për të bërë kopje rezervë, për shembull [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) në Ubuntu. Nëse një mjet i integruar për të bërë kopje rezervë është i përfshirë në sistemin tuaj operativ, mund t'ju jetë kërkuar që të aktivizoni bërjen e kopjes rezervë në mënyrë automatike kur keni filluar ta përdorni kompjuterin. Kërkoni versionin tuaj të Linux për të parë nëse ai përfshin një mjet të integruar për të bërë kopje rezervë dhe cila është procedura për të kontrolluar nëse ai është aktivizuar dhe për t'i rikthyer të dhënat nga kopjet rezervë.
>
> Për Déja Dup në Ubuntu, mund ta shihni këtë [artikull](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html) për të kontrolluar nëse e keni të aktivizuar bërjen automatike të kopjeve rezervë në makinën tuaj, dhe në [këtë faqe](https://wiki.gnome.org/DejaDup/Help/Restore/Full) për udhëzime se si t'i riktheni të dhënat tuaja të humbura nga një kopje rezervë ekzistuese.

A ishte i dobishëm ky informacion për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

- [Po](#resolved_end)
- [Jo](#tech-assistance_end)

### mobile

Çfarë lloj sistemi operativ funksionon në celularin tuaj?

- [iOS](#ios-phone)
- [Android](#android-phone)


### ios-phone

> Ju mund të keni aktivizuar bërjen automatike të kopjes rezervë me iCloud ose iTunes. Lexoni [këtë udhëzues](https://support.apple.com/kb/ph12521?locale=en_US) për të kontrolluar nëse keni ndonjë kopje rezervë ekzistuese dhe për të mësuar se si t'i riktheni të dhënat tuaja.

A e keni gjetur kopjen tuaj rezervë tuaj dhe a i keni rikuperuar të dhënat tuaja?

- [Po](#resolved_end)
- [Jo](#phone-which-data)

###  phone-which-data

Cila nga deklaratat e mëposhtme vlen për të dhënat që keni humbur?

- [Janë të dhëna të krijuara nga aplikacionet, p.sh. kontakte, burime (feeds), etj.](#app-data-phone)
- [Janë të dhëna të krijuara nga përdoruesi, p.sh. foto, video, audio, shënime](#ugd-phone)

### app-data-phone

> Një aplikacion celular është një aplikacion softuerik i dizajnuar për t'u ekzekutuar në pajisjen tuaj celulare. Megjithatë, në shumicën e këtyre aplikacioneve mund të hyhet edhe përmes një shfletuesi desktop. Nëse keni humbur të dhënat e krijuara nga aplikacionet në celularët tuaj, përpiquni të hyni në atë aplikacion përmes shfletuesit tuaj në desktop, duke hyrë në ndërfaqen e aplikacionit në internet me kredencialet për atë aplikacion. Ndoshta mund t'i gjeni të dhënat që i keni humbur në ndërfaqen e shfletuesit.

A ishte i dobishëm ky informacion për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

- [Po](#resolved_end)
- [Jo](#tech-assistance_end)

### ugd-phone

> Të dhënat e krijuara nga përdoruesi janë lloj i të dhënave që i krijoni ose i gjeneroni përmes një aplikacioni specifik, dhe në rast të humbjes së të dhënave do të doni të kontrolloni nëse ai aplikacion i ka cilësimet për të krijuar kopje rezervë të aktivizuara si parazgjedhje ose nëse mundëson një mënyrë për ta rikuperuar atë. Për shembull, nëse përdorni WhatsApp në celularin tuaj dhe bisedat u janë humbur ose ka ndodhur ndonjë gabim, mund t'i rikuperoni bisedat nëse e aktivizoni cilësimin e WhatsApp-it për bërje të kopjes rezervë, ose nëse përdorni një aplikacion për të krijuar dhe mbajtur shënime me informacione të ndjeshme ose personale, në të gjithashtu mund të jetë aktivizuar opsioni për të bërë kopje rezervë pa e vërejtur ju këtë ndonjëherë.

A ishte i dobishëm ky informacion për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

- [Po](#resolved_end)
- [Jo](#tech-assistance_end)

### android-phone

> Google ka një shërbim të integruar në Android, i quajtur Android Backup Service. Si parazgjedhje, ky shërbim bën kopje rezervë të disa llojeve të të dhënave dhe i shoqëron ato me shërbimin e duhur të Google, ku gjithashtu mund t'u qaseni në internet. Në shumicën e pajisjeve Android, ju mund të shihni cilësimet tuaj të sinkronizimit duke shkuar në u drejtuar te Settings > Accounts > Google, dhe më pas duke e zgjedhur adresën tuaj Gmail. Nëse keni humbur të dhëna të llojit që jeni duke i sinkronizuar me llogarinë tuaj të Google, me siguri do të jeni në gjendje t'i rikuperoni ato duke hyrë në llogarinë tuaj të Google përmes një shfletuesi të internetit.

A ishte i dobishëm ky informacion për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

- [Po](#resolved_end)
- [Jo](#phone-which-data)


### tech-assistance_end

> Nëse humbja e të dhënave tuaja është shkaktuar nga dëme fizike siç janë rënia e pajisjeve në dysheme ose në ujë, ekspozimi ndaj ndërprerjes së energjisë elektrike ose çështje të tjera, situata më e mundshme është se do t'ju duhet të bëni një rikuperim të të dhënave të ruajtura në pajisjen tuaj. Nëse nuk dini si t'i kryeni këto operacione, duhet të kontaktoni një person të TI që ka mjete për mirëmbajtjen e pajisjeve elektronike, e cila mund t'ju ndihmojë. Megjithatë, në varësi të kontekstit tuaj dhe ndjeshmërisë së të dhënave që doni t'i rikuperoni, nuk ju këshillojmë të kontaktoni ndonjë dyqan të TI, por të përpiqeni t'u jepni përparësi personave të TI që i njihni dhe u besoni.


### device-lost-theft_end

> Në rast të pajisjeve të humbura, të vjedhura ose të konfiskuara, sigurohuni që të ndryshoni sa më shpejt të gjitha fjalëkalimet tuaja dhe të vizitoni [burimin tonë specifik se çfarë të bëni nëse keni humbur ndonjë pajisje](../../../I lost my devices).
>
> Nëse jeni anëtar i shoqërisë civile dhe keni nevojë për mbështetje për të marrë një pajisje të re për ta zëvendësuar pajisjen e humbur, mund t'i drejtoheni organizatave të listuara më poshtë.

:[](organisations?services=equipment-replacement)


### storage-devices_end

> Në mënyrë që të rikuperoni të dhënat që keni humbur, mbani mend se koha është e rëndësishme. Për shembull rikuperimi i një skedari që e keni fshirë aksidentalisht disa orë ose një ditë më parë mund të ketë gjasa më të mëdha për t'u rikuperuar sesa një skedar që e keni humbur muaj më parë.
>
> Konsideroni të përdorni një mjet softuerik për të provuar të rikuperoni të dhënat që keni humbur (për shkak të fshirjes ose dëmtimit) siç është [Recuva për Windows] (https://www.ccleaner.com/recuva/download), ndërsa për Gnu/Linux, mund të kërkoni programe të ndryshme që janë të disponueshme për versionin tuaj. Merrni parasysh që këto mjete nuk funksionojnë gjithmonë, sepse sistemi juaj operativ mund të ketë shkruar të dhëna të reja mbi informacionin tuaj të fshirë. Për shkak të kësaj, duhet të punoni sa më pak të jetë e mundur me kompjuterin tuaj nga momenti i fshirjes së një skedari deri në momentin e përpjekjes për ta rikthyer atë me një mjet si Recuva.
>
> Për të rritur shanset për rikuperimin e të dhënave, ndaloni menjëherë përdorimin e pajisjeve tuaja. Shkrimi i vazhdueshëm në hard disk mund të zvogëlojë shanset për të gjetur dhe rikthyer të dhënat.
>
> Nëse asnjë nga opsionet e mësipërme nuk ka funksionuar për ju, situata më e mundshme është se do t'ju duhet të bëni një rikuperim të të dhënave të ruajtura në hard disk.  Nëse nuk dini si t'i kryeni këto operacione, duhet të kontaktoni një person të TI që ka mjete për mirëmbajtjen e pajisjeve elektronike, i cili mund t'ju ndihmojë. Megjithatë, në varësi të kontekstit tuaj dhe ndjeshmërisë së të dhënave që doni t'i rikuperoni, ne nuk ju këshillojmë të kontaktoni ndonjë dyqan të TI, por të përpiqeni t'u jeni përparësi personave të TI që i njihni dhe u besoni.


### resolved_end

Shpresojmë që ky udhëzues i DFAK të ishte i dobishëm. Ju lutemi na jepni komentet tuaja [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Kopjet rezervë - Përveç këshillave të ndryshme më lart, është gjithmonë ide e mirë të siguroheni se keni kopje rezervë - kopje rezervë të ndryshme, që i ruani diku tjetër nga vendi ku janë të dhënat tuaja! Në varësi të kontekstit tuaj, vendosni të ruani kopjen tuaj rezervë në shërbimet në re (cloud) dhe në pajisjet fizike të jashtme që i mbani të shkëputura nga kompjuteri juaj derisa jeni të lidhur në internet.
- Për të dyja llojet e kopjeve rezervë, duhet t'i mbroni të dhënat me enkriptim. Bëni kopje rezervë të rregullt dhe të vazhdueshme të të dhënave më të rëndësishme dhe kontrolloni nëse i keni ato gati përpara se të kryeni azhurnime të softuerëve ose të sistemit operativ.
- Vendosni një sistem të strukturuar të dosjeve - çdo person ka mënyrën e vet të organizimit të të dhënave dhe të informacioneve të tyre të rëndësishme, nuk ka mënyrë që  përshtatet të gjithëve. Megjithatë, është e rëndësishme që të merrni në konsideratë krijimin e një sistemi dosjeje që i përshtatet nevojave tuaja. Duke krijuar një sistem dosjeje të qëndrueshëm, mund ta bëni jetën tuaj më të lehtë duke e ditur më mirë, për shembull se për cilat dosje dhe cilët skedarë duhet të bëhen kopje rezervë të shpeshta, ku gjenden informacionet e rëndësishme me të cilat punoni, ku duhet t'i mbani të dhënat që përmbajnë informacion personal ose të ndjeshëm për ju dhe bashkëpunëtorët tuaj, etj. Si zakonisht, merrni frymë thellë dhe ndani kohë për të planifikuar llojin e të dhënave që prodhoni ose menaxhoni, dhe mendoni për një sistem dosje që mund ta bëjë atë më të qëndrueshëm dhe më të rregulluar.

#### burimet

- [Siguria në një kuti - taktikat për të bërë kopje rezervë](https://securityinabox.org/en/guide/backup/)
- [Faqja zyrtare për kopje rezervë e Mac](https://support.apple.com/mac-backup)
- [Si të bëni kopje rezervë të të dhënave rregullisht në Windows 10](https://support.microsoft.com/en-hk/help/4027408/windows-10-backup-and-restore)
- [Si ta aktivizoni bërjen e rregullt të kopjeve rezervë në iPhone](https://support.apple.com/en-us/HT203977)
- [Si ta aktivizoni bërjen e rregullt të kopjeve rezervë në Android](https://support.google.com/android/answer/2819582?hl=en).
