---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: 8AM-18PM Mon-Sun CET
response_time: 4 hours
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation is a Security Solutions Provider to independent media, human rights organizations, investigative journalists and activists. Qurium provides a portfolio of professional, customized and secure solutions with personal support to organizations and individuals at risk, which includes:

- Secure Hosting with DDoS mitigation of at-risk websites
- Rapid Response support to organizations and individuals under immediate threat
- Security audits of web services and mobile apps
- Circumvention of Internet blocked websites
- Forensics investigations of digital attacks, fraudulent Apps, targeted malware and disinformation